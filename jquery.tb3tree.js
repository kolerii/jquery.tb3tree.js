/**
 @version: 1.2.0
 @author : Beliakov Vladislav [kolerii]
 @url    : http://kolerii.ru/?p=310
*/

(function($){
  var tb3Tree = function(element, options){
    this.$element = $(element);
    this.options  = $.extend({}, $.fn.tb3tree.defaults, options);

    this.init();

    this.$element.on('click','.tree-node', $.proxy(function(e){this._click(e)},this));
    this.$element.on('click','span.tree-dir', $.proxy(function(e){this._click_dir(e)},this));
  }

  tb3Tree.prototype = {
    init : function(){
      this.$element.attr('unselectable','on').css({
                      '-moz-user-select': 'none',
                      '-khtml-user-select': 'none',
                      '-webkit-user-select': 'none',
                      '-o-user-select': 'none',
                      'user-select': 'none',
                      'MozUserSelect': 'none',
                      'border':'1px solid rgb(187, 187, 187)',
                      'border-radius':'4px 4px 4px 4px'
                    });
      this.$element.html( this.options.start_string + '<ul>' + this._build_function(this.options.data) + '</ul>' );
    },
    show : function( string ){
      if( string == '' ){
        this.$element.find('.tree-node').show();
        this.$element.find('span.tree-dir').each(function(){$(this).parent().show()});
      }else{
        this.$element.find('.tree-node').hide();
        this.$element.find('.tree-node:contains("'+string+'")').show();
        this.$element.find('span.tree-dir + ul').each(function(){
          if($(this).find('.tree-node[style!="display: none;"]').length == 0)
            $(this).parent().hide();
          else
            $(this).parent().show();
        });
      }
    },
    expand : function(){
      this.show('');
      this.$element.find('li[displayed="false"]').attr('displayed','true');
      this.$element.find('li[displayed="true"] > ul').show();
    },
    reduce : function(){
      this.show('');
      this.$element.find('li[displayed="true"]').attr('displayed','false');
      this.$element.find('li[displayed="false"] > ul').hide();
    },
    get : function( param ){
      var arr = [];

      this.$element.find('.tree-node[checked="checked"]').each(function(){
        if( param == 'html' )
          arr.push($(this).html());
        else
          arr.push($(this).attr(param));
      });

      return arr;
    },
    _click : function(object){
      if(this.options.type == 'radio')
        this.$element.find('.tree-node[checked="checked"]').removeAttr('checked');
      if(this.options.type != 'none')
        $(object.currentTarget).attr('checked') ? $(object.currentTarget).removeAttr('checked') : $(object.currentTarget).attr('checked', 'checked');
      if(typeof this.options.callback == 'function' )
        this.options.callback($(object.currentTarget));
    },
    _click_dir : function(object){
      var next = $(object.currentTarget).next(), parent = $(object.currentTarget).parent();

      if(parent.attr('displayed') == 'true'){
        parent.attr('displayed', false);
        next.hide()
      }
      else{
        parent.attr('displayed', true);
        next.show()
      }
    },
    destroy : function(){
      this.$element.html('').removeAttr('unselectable').css({
                      '-moz-user-select': '',
                      '-khtml-user-select': '',
                      '-webkit-user-select': '',
                      '-o-user-select': '',
                      'user-select': '',
                      'MozUserSelect': '',
                      'border':'',
                      'border-radius':''
      });
    },
    _createElement : function( object, defaults, tagName )
    {
        var currentElement = document.createElement(tagName);

        for( var i in object.attrs )
        {
          if(!defaults.hasOwnProperty(i)) defaults[i] = [];
          if(typeof(object.attrs[i]) == 'string')
            defaults[i].push(object.attrs[i]);
        }

        for( var i in defaults ) currentElement.setAttribute(i, defaults[i].join(' '));
        currentElement.innerHTML = object.title;

        return currentElement;
    },
    _build_function : function( object ){
      var type = Object.prototype.toString.call( object );
      if( type == '[object Array]')
      {
        var return_html = '';
        for( var x in object )
        {
          if (!object.hasOwnProperty(x)) continue;
          return_html += this._build_function(object[x]);
        }
        return return_html;
      }
      else if(type == '[object Object]' && object.hasOwnProperty('sub'))
      {
        var deployed = object.hasOwnProperty('deployed') && object.deployed,
            currentSpan = this._createElement(object, {'class':['tree-dir']}, 'span');

        return '<li displayed="'+deployed.toString()+'">'+currentSpan.outerHTML+'<ul style="'+ (deployed ? 'display: block;' : 'display:none;')+'">'+this._build_function(object.sub)+'</ul></li>';
      }
      else if(type == '[object Object]' && !object.hasOwnProperty('sub') && object.hasOwnProperty('title'))
        return currentLi = this._createElement(object, {'class':['tree-node']}, 'li').outerHTML;
      else
        return '<li class="tree-node">'+object+'</li>';
    } 
  };

  jQuery.fn.tb3tree = function(option, value)
  {
    var methodReturn;

    var $set = this.each(function (){
      var $this = $(this);
      var data = $this.data('tb3tree');
      var options = typeof option === 'object' && option;


      if (!data) $this.data('tb3tree', (data = new tb3Tree(this, options)));

      if (typeof option === 'string') methodReturn = data[option](value);
    });

    return methodReturn === undefined ? $set : methodReturn;
  };

  jQuery.fn.tb3tree.defaults = {
          type : 'checkbox',
          data : [],
          callback : false,
          start_string : ''
        }
})(jQuery);